let inputElement = document.querySelector("#input");
let aboveInput = document.querySelector("#aboveInput");
let button = document.querySelector("#button");
let underInput = document.querySelector("#underInput");
aboveInput.style.display = "none";
button.style.display = "none";
underInput.style.display = "none";

inputElement.addEventListener("focus", () => {
    inputElement.style.outlineColor = "green";
})
inputElement.addEventListener("blur", () => {
    if (inputElement.value === "") {
        inputElement.style.outlineColor = "black";
    }
    else if (inputElement.value >= 0) {
        inputElement.style.color = "green";
        aboveInput.style.display = "inline-block";
        aboveInput.innerHTML = `Поточна ціна: ${inputElement.value} USD`
        button.style.display = "inline-block";
        underInput.innerHTML = "";
    } else {
        underInput.style.display = "inline-block";
        inputElement.style.border = "1px solid red";
        inputElement.style.color = "black";
        underInput.innerHTML = "Please enter correct price";
        aboveInput.style.display = "none";
        button.style.display = "none";
    }
})
button.addEventListener("click", () => {
    aboveInput.style.display = "none";
    button.style.display = "none";
    inputElement.value = "";
})

